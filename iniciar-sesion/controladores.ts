import Controlador from "../helpers/Controlador";
import bcrypt from "bcrypt";
import { Usuario } from "./../models/usuario.model";
import jwt from "jsonwebtoken";
import config from "config";

export class controllerIniciarSesion extends Controlador {
  run() {
    let response;
    let body: any = "";
    this.req.on("data", function(chunk: any) {
      body += chunk;
    });
    this.req.on("end", async () => {
      let { correo, password } = JSON.parse(body);
      let usuario = await Usuario.findOne({ correo });
      // @ts-ignore
      if (usuario && bcrypt.compareSync(password, usuario.password)) {
        // @ts-ignore
        const token = usuario.generarAuthToken();
        // @ts-ignore
        usuario.token = token;
        usuario.save();
        const { password, ...usuarioWithoutPassword } = usuario.toObject();
        response = {
          ...usuarioWithoutPassword
        };
        this.response(response);
      } else {
        this.setStatusCode(401);
        this.response("Datos incorrectos");
      }
    });
  }
}

export class controllerCerrarSesion extends Controlador {
  async run() {
    const token =
      // @ts-ignore
      this.req.headers["x-access-token"] || this.req.headers["authorization"];
    try {
      const decoded = jwt.verify(token.split(" ")[1], config.get("myKey"));
      // @ts-ignore
      const usuario = await Usuario.findById(decoded._id);
      // @ts-ignore
      if (usuario.token != "") {
        // @ts-ignore
        usuario.token = "";
        // @ts-ignore
        usuario.save();
        this.response({ res: "Sesión cerrada" });
      } else {
        this.setStatusCode(400);
        this.response({ error: "Token invalido." });
      }
    } catch (ex) {
      this.setStatusCode(400);
      this.response({ error: "Token invalido." });
    }
  }
}
