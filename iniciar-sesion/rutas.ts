import { iniciarSesionServer } from "./servidor";
import { controllerIniciarSesion, controllerCerrarSesion } from "./controladores";

iniciarSesionServer.route("post", "/iniciarSesion", controllerIniciarSesion);
iniciarSesionServer.route("post", "/cerrarSesion", controllerCerrarSesion);
