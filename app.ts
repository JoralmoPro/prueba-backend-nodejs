import { registroServer } from "./registro/servidor";
import { iniciarSesionServer } from "./iniciar-sesion/servidor";
import { palindromoServer } from "./palindromo/servidor";
import { historicoServer } from "./historico/servidor";
import mongoose from "mongoose";

mongoose
  .connect("mongodb://localhost:27017/auth", { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log("Conectado a MongoDB"))
  .catch(err => console.error("No se pudo conectar a MongoDB..."));

registroServer.start();
iniciarSesionServer.start();
palindromoServer.start();
historicoServer.start();