import config from "config";
import jwt from "jsonwebtoken";
import joi from "joi";
import mongoose from "mongoose";

const UsuarioSchema = new mongoose.Schema({
  nombre: {
    type: String,
    required: true,
    minlength: 3,
    maxlength: 50
  },
  correo: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 255,
    unique: true
  },
  password: {
    type: String,
    required: true,
    minlength: 3,
    maxlength: 255
  },
  esAdmin: Boolean,
  token: String
});

UsuarioSchema.methods.generarAuthToken = function() {
  const token = jwt.sign(
    { _id: this._id, esAdmin: this.esAdmin },
    config.get("myKey"),
    { expiresIn: "1h" }
  );
  return token;
};

export const Usuario = mongoose.model("Usuario", UsuarioSchema);

export function validarUsuario(usuario) {
  const schema = {
    nombre: joi
      .string()
      .min(3)
      .max(50)
      .required(),
    correo: joi
      .string()
      .min(5)
      .max(255)
      .required()
      .email(),
    password: joi
      .string()
      .min(3)
      .max(255)
      .required(),
    esAdmin: joi.boolean().required(),
    token: joi.string()
  };

  return joi.validate(usuario, schema);
}
