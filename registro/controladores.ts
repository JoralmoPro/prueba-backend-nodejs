import Controlador from "../helpers/Controlador";
import { validarUsuario, Usuario } from "./../models/usuario.model";
import bcrypt from "bcrypt";

export class controllerRegistro extends Controlador {
  run() {
    let response;
    let body: any = "";
    this.req.on("data", function(chunk: any) {
      body += chunk;
    });
    this.req.on("end", async () => {
      let usuarioNuevo: any = JSON.parse(body);

      const { error } = validarUsuario(usuarioNuevo);

      if (error) {
        response = {
          error: error.details[0].message
        };
        this.setStatusCode(400);
        return this.response(response);
      }

      let { nombre, correo, password, esAdmin } = usuarioNuevo;
      let usuario = await Usuario.findOne({ correo });
      if (usuario) {
        response = {
          error: "El usuario se encuentra registrado."
        };
        this.setStatusCode(400);
        return this.response(response);
      }

      password = await bcrypt.hash(password, 10);
      usuario = new Usuario({
        nombre,
        correo,
        password,
        esAdmin,
        token: ""
      });

      await usuario.save();

      // @ts-ignore
      const token: string = usuario.generarAuthToken();

      response = {
        _id: usuario._id,
        nombre,
        correo
      };

      this.setHeader("x-auth-token", token);
      this.response(response);
    });
  }
}
