import { registroServer } from "./servidor";
import { controllerRegistro } from "./controladores";

registroServer.route("post", "/registro", controllerRegistro);
