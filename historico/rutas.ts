import { historicoServer } from "./servidor";
import { controllerNuevaPalabra } from "./controladores";

historicoServer.route("post", "/guardarPalabra", controllerNuevaPalabra);
