import Controlador from "../helpers/Controlador";

export class controllerNuevaPalabra extends Controlador {
  run() {
    let response;
    let body: any = "";
    this.req.on("data", function(chunk: any) {
      body += chunk;
    });
    this.req.on("end", async () => {
      const { palabra, reverse, usuario } = JSON.parse(body);
      this.response({ palabra, reverse, usuario, hola: "Hola" });
    });
  }
}
