import Controlador from "../helpers/Controlador";
import { Usuario } from "./../models/usuario.model";
import jwt from "jsonwebtoken";
import config from "config";
import http from "http";
import enviromentHist from "./../historico/enviroments";

export class controllerPalindromo extends Controlador {
  run() {
    let response;
    let body: any = "";
    this.req.on("data", function(chunk: any) {
      body += chunk;
    });
    this.req.on("end", async () => {
      const token =
        // @ts-ignore
        this.req.headers["x-access-token"] || this.req.headers["authorization"];
      try {
        const decoded = jwt.verify(token.split(" ")[1], config.get("myKey"));
        // @ts-ignore
        const usuario = await Usuario.findById(decoded._id);

        // @ts-ignore
        if (usuario.token == "") {
          this.setStatusCode(401);
          return this.response({ error: "Token invalido" });
        }

        const { palabra } = JSON.parse(body);
        if (typeof palabra == "string") {
          let reverse = palabra
            .split("")
            .reverse()
            .join("");
          let palindromo = palabra == reverse ? true : false;

          const opciones = {
            hostname: enviromentHist.host,
            port: enviromentHist.puerto,
            path: "/guardarPalabra",
            method: "POST",
            headers: {
              "Content-Type": "application/json"
            }
          };

          let b = "";
          const requ = http.request(opciones, resp => {
            console.log("resprespresprespresprespresprespresp");
            resp.on("data", d => {
              b += d;
            });
            resp.on("end", () => {
              console.log(JSON.parse(b).hola);
            });
            console.log(resp.statusCode);
            console.log("resprespresprespresprespresprespresp");
          });

          requ.write(JSON.stringify({ palabra, reverse, usuario }));
          requ.end();

          this.response({
            palindromo,
            palabra,
            reverse,
            // @ts-ignore
            usuario: usuario.nombre
          });
        } else {
          this.setStatusCode(503);
          this.response({ error: "El parametro recibido no es una palabra" });
        }
      } catch (error) {
        this.response({ error: "Token invalido" });
      }
    });
  }
}
