import { palindromoServer } from "./servidor";
import { controllerPalindromo } from "./controladores";

palindromoServer.route("post", "/palindromo", controllerPalindromo);
