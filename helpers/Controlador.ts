import { ServerResponse, ClientRequest } from "http";
export default class Controlador {
  req: ClientRequest;
  res: ServerResponse;
  reqUrl: Object;

  constructor(req: ClientRequest, res: ServerResponse, reqUrl: Object) {
    this.req = req;
    this.res = res;
    this.reqUrl = reqUrl;

    this.res.statusCode = 200;
    this.res.setHeader("Content-Type", "application/json");
  }

  public setStatusCode(code: number) {
    this.res.statusCode = code;
  }

  public setHeader(head: string, value: string) {
    this.res.setHeader(head, value);
  }

  private json(data: any) {
    return JSON.stringify(data);
  }

  public response(data: any = null) {
    if (data != null) {
      this.res.end(this.json(data));
    } else {
      this.res.end();
    }
  }
}
