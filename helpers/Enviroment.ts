export default function() {
  let mode = "";
  process.argv.forEach((arg, i) => {
    if (arg.toLocaleLowerCase() == "--mode") mode = process.argv[i + 1];
  });
  return mode;
}
