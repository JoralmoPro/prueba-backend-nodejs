import http from "http";
import url from "url";

export default class Servidor {
  private host: string;
  private puerto: number;
  private rutas: Object;
  private _servidor: any;

  constructor(host: string, port: number) {
    this.host = host;
    this.puerto = port;
    this.rutas = { GET: [], POST: [] };
  }

  public route(metodo: string, path: string, controlador: Function) {
    this.rutas[metodo.toLocaleUpperCase()].push({
      path,
      controlador
    });
  }

  public start() {
    this._servidor = http.createServer(
      // @ts-ignore
      (req: http.ClientRequest, res: http.ServerResponse) => {
        // @ts-ignore
        const reqUrl = url.parse(req.url, true);
        // @ts-ignore
        this.rutas[req.method].forEach(ruta => {
          if (ruta.path == reqUrl.pathname) {
            new ruta.controlador(req, res, reqUrl).run();
          }
        });
      }
    );

    this._servidor.listen(this.puerto, this.host, () => {
      console.log(`Servidor lanzado en http://${this.host}:${this.puerto}/`);
    });
  }
}
